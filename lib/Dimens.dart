class Dimens {
  static final double viewS = 4.0;
  static final double viewM = 8.0;
  static final double viewL = 12.0;
  static final double viewXL = 16.0;
  static final double viewXXL = 16.0;
  static final double viewXXXL = 16.0;

  static final double textS = 12.0;
  static final double textM = 16.0;
  static final double textL = 20.0;
  static final double textXL = 24.0;
  static final double textXXL = 28.0;
  static final double textXXXL = 32.0;

  static final double spaceS = 12.0;
  static final double spaceM = 16.0;
  static final double spaceL = 24.0;
  static final double spaceXL = 30.0;
}