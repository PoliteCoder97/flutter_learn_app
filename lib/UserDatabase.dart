import 'package:firstflutterlearn/MyDatabase.dart';
import 'package:firstflutterlearn/User.dart';

class UserDatabase extends MyDatabase {
  final String table = 'users';
  static UserDatabase? _instance;

  static UserDatabase? getInstance() {
    if (_instance == null) {
      _instance = UserDatabase();
    }
    return _instance;
  }

  UserDatabase() {}

  Future<User?> insert(User user) async {
    final db = await getDb();
    int recordId = await db!.insert(table, user.toMap());
    user.id = recordId;
    return user;
  }

  Future<int> update(User user) async {
    User? storedUser = await this.getUser(user.id);
    if (storedUser == null) return -1;
    final db = await getDb();
    int recordId = await db!
        .update(table, user.toMap(), where: 'id = ?', whereArgs: [user.id]);
    return recordId;
  }

  Future<User?> getUser(int id) async {
    final db = await getDb();
    List<Map<String, dynamic>> maps = await db!.query(table,
        columns: [
          'id',
          'name',
          'family',
          'mobile',
          'email',
          'address',
        ],
        where: 'id = ?',
        whereArgs: [id]);
    if (maps.length > 0) {
      return User.fromMap(maps.first);
    }
    return null;
  }

  Future<User?> getLastUser() async {
    final db = await getDb();

    List<Map<String, dynamic>> maps =
        await db!.rawQuery('SELECT * FROM users ORDER BY id DESC LIMIT 1');

    if (maps.length > 0) {
      return User.fromMap(maps.first);
    }
    return null;
  }

  Future<void> delete(int id) async {
    final db = await getDb();
    await db!.delete(table, where: 'id = ?', whereArgs: [id]);
  }
}
