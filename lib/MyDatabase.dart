import 'dart:async';

import 'package:sqflite/sqflite.dart';

class MyDatabase{
  int _dbVersion = 1;
  Database? _db;

  Future<Database?> getDb() async {
    if (_db != null) return _db;

    _db = await makeDb();
    return _db;
  }

  Future close() async {
    var db = await getDb();
//    db.close();
  }

  makeDb() async {
    var dbPath = await getDatabasesPath();
    dbPath = '$dbPath/mydb.db';
    // var path = join(dbPath, );

    var myDb = await openDatabase(dbPath,
        version: _dbVersion, onCreate: onCreate, onUpgrade: onUpgrade);
    return myDb;
  }

  FutureOr<void> onUpgrade(Database db, int oldVersion, int newVersion) {
  }

  FutureOr<void> onCreate(Database db, int version) {
    try {
      db.execute(
          'CREATE TABLE IF NOT EXISTS users (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, family TEXT, mobile TEXT , email TEXT, address TEXT)');
    }catch(e){
      print(e);
    }
  }
}