class User {
  int id;
  final String name;
  final String family;
  final String mobile;
  final String email;
  final String address;

  User({
    required this.id,
    required this.name,
    required this.family,
    required this.mobile,
    required this.email,
    required this.address,
  });

  factory User.fromMap(Map<String, dynamic> json) {
    return User(
      id: json['id'] ?? 0,
      name: json['name'] ?? '-',
      family: json['family'] ?? '-',
      mobile: json['mobile'] ?? '-',
      email: json['email'] ?? '-',
      address: json['address'] ?? '-',
    );
  }

  Map<String, dynamic> toMap() {
    return {
      // 'id': this.id,
      'name': this.name ?? '',
      'family': this.family ?? '',
      'mobile': this.mobile ?? '',
      'email': this.email ?? '',
      'address': this.address ?? '',
    };
  }
}
