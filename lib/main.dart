import 'package:firstflutterlearn/User.dart';
import 'package:firstflutterlearn/UserDatabase.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'Dimens.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primaryColor: Colors.red,
          visualDensity: VisualDensity(horizontal: 3, vertical: 3),
          brightness: Brightness.light,
          primaryColorDark: Colors.purple,
          textTheme: TextTheme(
            headline1: TextStyle(
              fontSize: Dimens.textXXXL,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
            headline2: TextStyle(
              fontSize: Dimens.textXXL,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
            headline3: TextStyle(
              fontSize: Dimens.textXL,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
            headline4: TextStyle(
              fontSize: Dimens.textL,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
            headline5: TextStyle(
              fontSize: Dimens.textM,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
            headline6: TextStyle(
              fontSize: Dimens.textS,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          )),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  GlobalKey<ScaffoldState> _key = GlobalKey<ScaffoldState>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  TextEditingController _nameController = TextEditingController();
  TextEditingController _familyController = TextEditingController();
  TextEditingController _phonNumberController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _addressController = TextEditingController();

  @override
  void initState() {
    super.initState();
    loadData();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _key,
      appBar: AppBar(
        title: Text('PoliteCoder'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    'Profile Form',
                    style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Form(
                key: _formKey,
                child: Column(
                  children: [
                    MyTextFormField(
                      context,
                      _nameController,
                      'Name',
                      (value) {
                        if (value == null || value.isEmpty)
                          return 'مقدار نام را وارد کنید';
                        if (value.length < 3)
                          return 'مقدار نام باید بیشتر از 3 کاراکتر باشد';
                      },
                      style: Theme.of(context).textTheme.headline1,
                    ),
                    MyTextFormField(
                      context,
                      _familyController,
                      'Family',
                      (value) {
                        if (value == null || value.isEmpty)
                          return 'مقدار فامیلی را وارد کنید';
                        if (value.length < 3)
                          return 'مقدار نام باید بیشتر از 3 کاراکتر باشد';
                      },
                      textInputType: TextInputType.text,
                    ),
                    MyTextFormField(context, _phonNumberController, 'Mobile',
                        (value) {
                      if (value == null || value.isEmpty)
                        return 'مقدار تلفن همراه را وارد کنید';
                      if (value.length < 11)
                        return 'مقدار شماره همراه را ب درستی وارد کنید';
                      if (!value.startsWith('09'))
                        return 'مقدار شماره همراه را ب درستی وارد کنید';
                    }, textInputType: TextInputType.phone),
                    MyTextFormField(context, _emailController, 'Email',
                        (value) {
                      if (value == null || value.isEmpty)
                        return 'مقدار ایمیل را وارد کنید';
                      if (!value.contains('@'))
                        return 'مقدار ایمیل را به درستی وارد کنید ';
                    }, textInputType: TextInputType.emailAddress),
                    MyTextFormField(context, _addressController, 'Address',
                        (value) {
                      if (value == null || value.isEmpty)
                        return 'مقدار ادرس را وارد کنید';
                    }),
                  ],
                ),
              ),
              InkWell(
                onTap: () async {
                  if (!_formKey.currentState!.validate()) {
                    return;
                  }

                  // SharedPreferences prefs =
                  //     await SharedPreferences.getInstance();
                  //
                  // await prefs.setString('Name', _nameController.text);
                  // await prefs.setString('Family', _familyController.text);
                  // await prefs.setString('Mobile', _phonNumberController.text);
                  // await prefs.setString('Email', _emailController.text);
                  // await prefs.setString('Address', _addressController.text);

                  User? user = User(
                      id: 0,
                      name: _nameController.text,
                      family: _familyController.text,
                      mobile: _phonNumberController.text,
                      email: _emailController.text,
                      address: _addressController.text);

                  user = await UserDatabase.getInstance()!.insert(user);

                  if (user == null) {
                    ScaffoldMessenger.of(context).showSnackBar(
                      const SnackBar(
                        backgroundColor: Colors.red,
                        content: Text('خطا در ثبت کاربر'),
                      ),
                    );
                    return;
                  }

                  ScaffoldMessenger.of(context).showSnackBar(
                    const SnackBar(
                      backgroundColor: Colors.green,
                      content: Text('اطلاعات شما با موفقیت ذخیره شد'),
                    ),
                  );
                },
                child: Container(
                  height: 33,
                  width: double.infinity,
                  margin: EdgeInsets.symmetric(
                    horizontal: 12,
                    vertical: 8,
                  ),
                  decoration: BoxDecoration(
                    color: Colors.purple,
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Center(
                    child: Text(
                      'validate',
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> loadData() async {
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    //
    // String name = prefs.getString('Name') ?? '';
    // String family = prefs.getString('Family') ?? '';
    // String mobile = prefs.getString('Mobile') ?? '';
    // String email = prefs.getString('Email') ?? '';
    // String address = prefs.getString('Address') ?? '';

    User? user = await UserDatabase.getInstance()!.getLastUser();

    String name = user!.name ?? '';
    String family = user!.family ?? '';
    String mobile = user!.mobile ?? '';
    String email = user!.email ?? '';
    String address = user!.address ?? '';

    _nameController.text = name;
    _familyController.text = family;
    _phonNumberController.text = mobile;
    _emailController.text = email;
    _addressController.text = address;
  }
}

class MyTextFormField extends StatelessWidget {
  final TextEditingController _controller;
  final String lable;
  final FormFieldValidator<String>? validator;
  final TextInputType textInputType;
  TextStyle? style;

  MyTextFormField(
      BuildContext context, this._controller, this.lable, this.validator,
      {this.textInputType = TextInputType.text, this.style}) {
    if (style == null) {
      this.style = Theme.of(context).textTheme.headline4;
    } else {
      this.style = style;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 4),
      child: TextFormField(
        controller: _controller,
        validator: this.validator,
        keyboardType: this.textInputType,
        style: style,
        decoration: InputDecoration(
          icon: Icon(Icons.person),
          labelText: lable,
          focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: Colors.purple,
          )),
          border: OutlineInputBorder(
              borderSide: BorderSide(
            color: Colors.grey,
          )),
          disabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: Colors.grey,
          )),
          enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: Colors.purple,
          )),
          errorBorder: OutlineInputBorder(
              borderSide: BorderSide(
            color: Colors.red,
          )),
        ),
      ),
    );
  }
}
